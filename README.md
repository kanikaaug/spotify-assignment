### Test Cases for Spotify Login functionality ###
    1) Test that user is able to login using valid user id and password and is taken to the landing page.
    2) Test that user is able to login with facebook.
    3) Test that user is able to login with apple.
    4) Test that user is not able to login with invalid username/password.
    5) Test that validation message is displayed if user does not enter anything for username/password fields.
    
### Test Cases for Spotify Sign Up Functionality ###
    1) Test that user is able to Sign Up For Spotify using-
       1.1) Facebook
       1.2) Email address
       
### Test Cases for Spotify Sign Up and Login Workflow with the new user created ###
    1) Sign Up for Spotify
    2) Verify that user is able to login with the username/email address created in the above step and is taken to the landing page.
    
### Test Cases Automated ###
    1) Test that user is able to login using valid user id and password and is taken to the landing page.
    2) Test that user is not able to login with invalid username/password.
    3) Test that user is able to login with facebook.

### Test Automation Framework ###
    I have followed page object model to automate the test cases. 
    All the page objects can be found under package `com.spotify.assignment.pageobjects`.
    All the test classes can be found under `com.spotify.assignment.tests`.
    
### Handling Test Data when Scaling ###

To make our test suite scalable there are coulple of ways we can handlle test data.


    1) Read the test data from external files(properties file or json files): 

	We can read the test data from external files and use it in our test suites.
	Each test suite can have its own test data file with all the required data.

	Pros:
	Test data and Test execution is decoupled so it is easy to run tests with different types of data.
	Easy to manage change in test data.

	Cons:
	If we are running tests in parallel, we need to make sure that we not are using any conflicting data.
	If our test suites keeps increasing, there could be lots of test data files to manage.

    2) Dynamically generate the data for test execution.

	In this approach we dynamically generate fresh data either through UI or by calling backend services.
	In the example here instead of reading manually created username and password from properties file, we can call beackend service to create a new user or 
	create new user thorugh singup flow on UI and use that newly created user for the test suite. This way each execution will have its own data to test.

	Pros:
	We do not have to reuse the same data again and again.
	Less psobility of confilcting data for parallel execution.

	Cons:
	Tests can be slow as we have to execute extra steps to generate data.
	
	3) We can also combine above methods and use both static data from external file and dynamically generated tets data.

### Cross browser testing ###

    I have utilized TestNG functionality to pass parameter from XML to implement cross browser testing.
    BaseTest class initializes appropriate driver based on the browser values passed from TestNG XML. 
    Current implementation runs the automated tests on chrome and firefox browser.

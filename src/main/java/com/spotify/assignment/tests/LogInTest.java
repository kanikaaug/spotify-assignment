package com.spotify.assignment.tests;

import com.spotify.assignment.pageobjects.LoginLandingPO;
import com.spotify.assignment.pageobjects.LogInPO;
import com.spotify.assignment.util.PropertyLoader;
import org.testng.Assert;
import org.testng.annotations.Test;

/***
 * LogIn Test verifies login with correct and incorrect username/password and also logs in using facebook
 */
public class LogInTest extends BaseTest {


    String loginUrl = PropertyLoader.loadProperty("login.url");

    @Test
    public void testLogin() {
        String username = PropertyLoader.loadProperty("username");
        String password = PropertyLoader.loadProperty("password");

        driver.get(loginUrl);
        LogInPO loginPage = new LogInPO(driver);
        loginPage.enterUsername(username);
        loginPage.enterPassword(password);
        loginPage.clickLoginButton();
        LoginLandingPO landingPage = new LoginLandingPO(driver);
        String loginMsg = landingPage.getLoginMessage();
        Assert.assertEquals(loginMsg, "Logged in as " + username + ".");
        landingPage.clickLogOut();
    }

    @Test
    public void testInCorrectLogin() {
        String username = PropertyLoader.loadProperty("username");
        String password = PropertyLoader.loadProperty("incorrect.password");

        driver.get(loginUrl);
        LogInPO loginPage = new LogInPO(driver);
        loginPage.enterUsername(username);
        loginPage.enterPassword(password);
        loginPage.clickLoginButton();
        String incorrectLoginMsg = loginPage.getIncorrectLoginMessage();
        Assert.assertEquals(incorrectLoginMsg,"Incorrect username or password.");
    }

    @Test
    public void testClickLoginWithFacebook() {
        driver.get(loginUrl);
        LogInPO loginPage = new LogInPO(driver);
        String facebookLoginUrl=loginPage.clickFacebookLoginButton();
        Assert.assertTrue(facebookLoginUrl.contains("facebook.com/login"));
    }
}

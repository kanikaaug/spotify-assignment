package com.spotify.assignment.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.*;

/***
 * This base test class implements generic methods to initialise WebDriver.
 */
public class BaseTest {

    protected WebDriver driver;

    @Parameters({"browser"})
    @BeforeTest
    public void setUp(String browser) {
        switch(browser){
            case "internetexplorer":
                System.setProperty("webdriver.ie.driver", "ie.path");
                driver = new InternetExplorerDriver();
                break;
            case "firefox":
                 System.setProperty("webdriver.gecko.driver", "geckodriver");
                 driver = new FirefoxDriver();
                 break;
            default:
                System.setProperty("webdriver.chrome.driver", "chromedriver");
                driver = new ChromeDriver();
                break;
        }
    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }

}

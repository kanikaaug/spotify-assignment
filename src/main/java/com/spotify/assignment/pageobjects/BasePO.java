package com.spotify.assignment.pageobjects;
import org.openqa.selenium.WebDriver;


/***
 * This is base page object class.
 */
public class BasePO {

    WebDriver driver;

    BasePO(WebDriver driver){
        this.driver=driver;
    }

}

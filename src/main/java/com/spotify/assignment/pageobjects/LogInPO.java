package com.spotify.assignment.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

/***
 * Page Object Class for Login page.
 */
public class LogInPO extends  BasePO {

    private String USERNAME_SELECTOR = "login-username";
    private String PASSWORD_SELECTOR = "login-password";
    private String LOGIN_SELECTOR = ".row-submit button";
    private String INCORRECT_PASSWORD = "p.alert";
    private String FACEBOOKBUTTON_SELECTOR=".btn-facebook";

    public LogInPO(WebDriver driver) {
        super(driver);
    }

    public void enterUsername(String username){
        driver.findElement(By.id(USERNAME_SELECTOR)).clear();
        driver.findElement(By.id(USERNAME_SELECTOR)).sendKeys(username);
    }

    public void enterPassword(String password){
        driver.findElement(By.id(PASSWORD_SELECTOR)).clear();
        driver.findElement(By.id(PASSWORD_SELECTOR)).sendKeys(password);
    }

    public void clickLoginButton(){
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.findElement((By.cssSelector(LOGIN_SELECTOR))).click();
    }

    public String clickFacebookLoginButton(){
        driver.findElement(By.cssSelector(FACEBOOKBUTTON_SELECTOR)).click();
        return driver.getCurrentUrl();
    }

    public String getIncorrectLoginMessage(){
       return driver.findElement((By.cssSelector(INCORRECT_PASSWORD))).getText();
    }

    public void clickLoginWithApple(){}

}


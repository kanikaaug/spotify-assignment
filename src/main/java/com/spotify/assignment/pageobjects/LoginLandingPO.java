package com.spotify.assignment.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/***
 * Page Object Class for Landing Page after successful login.
 */
public class LoginLandingPO extends BasePO {

    private String SUCCESSMSG_SELECTOR = "span.user-details";
    private String LOGOUT_SELECTOR = ".btn-block.simple-button";

    public LoginLandingPO(WebDriver driver) {
        super(driver);
    }

    public String getLoginMessage(){
        WebDriverWait wait = new WebDriverWait(driver,6);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(SUCCESSMSG_SELECTOR)));
        return driver.findElement((By.cssSelector(SUCCESSMSG_SELECTOR))).getText();
    }

    public void clickLogOut(){
        driver.findElement(By.cssSelector(LOGOUT_SELECTOR)).click();
    }
}

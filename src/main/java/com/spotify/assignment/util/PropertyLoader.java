package com.spotify.assignment.util;

import java.io.IOException;
import java.util.Properties;

/*
 * Provided if you would like to take advantage of the application.properties file found in src/main/resources/application.propertiess
 *
 */
public class PropertyLoader {

	private static final String PROP_FILE = "/application.properties";
        
	public PropertyLoader() {}

	public static String loadProperty(String name) {
		Properties props = new Properties();
		try {
			props.load(PropertyLoader.class.getResourceAsStream(PROP_FILE));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String value = "";

		if (name != null) {
			value = props.getProperty(name);
		}
		return value;
	}
}